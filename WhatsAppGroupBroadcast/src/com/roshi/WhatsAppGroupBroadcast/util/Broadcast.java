package com.roshi.WhatsAppGroupBroadcast.util;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javafx.collections.ObservableList;
import javafx.concurrent.Task;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.roshi.WhatsAppGroupBroadcast.standalone.StaticStrings;

public class Broadcast extends Task<Void>{

	WebDriver driver=StaticStrings.driver;
	private ObservableList<String> forwardingList;
	private String forwardMessage;
	private int type;
	
	public Broadcast(ObservableList<String> forwardingList,String forwardMessage,int type) {
		
		this.forwardingList=forwardingList;
		this.forwardMessage=forwardMessage;
		this.type=type;
	}
	
	
	@Override
	protected Void call()
	{
		try {
			
			int i=forwardingList.size();
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			
			if(type==1)
			{
				updateMessage("Forwarding to "+forwardingList.get(1));
				StaticStrings.fluentWait(By.cssSelector("#side > div.search-container > div > label > input")).clear();
				StaticStrings.fluentWait(By.cssSelector("#side > div.search-container > div > label > input")).sendKeys(forwardingList.get(1)+Keys.ENTER);
				StaticStrings.fluentWait(By.cssSelector("#main > footer > div > div.input-container > div > div.input")).clear();
				StaticStrings.fluentWait(By.cssSelector("#main > footer > div > div.input-container > div > div.input")).sendKeys(forwardMessage+Keys.ENTER);
				
				if(forwardingList.size()==2)
				{
					updateMessage("done.");
					updateProgress(1.0, 1.0);
					return null;
				}
				
				StaticStrings.fluentWait(By.cssSelector(".pane-chat-controls > div:nth-child(1) > div:nth-child(2) > button:nth-child(1)")).click();
				StaticStrings.fluentWait(By.cssSelector("li.menu-item:nth-child(2)")).click();			
				List<WebElement> messageList=driver.findElements(By.cssSelector(".message-list > div"));
				messageList.get(messageList.size()-1).click();
				StaticStrings.fluentWait(By.cssSelector("button.btn-icon:nth-child(4)")).click();

				i=2;
			}
			else if(type==2)
			{
				StaticStrings.fluentWait(By.cssSelector("#side > div.search-container > div > label > input")).clear();
				StaticStrings.fluentWait(By.cssSelector("#side > div.search-container > div > label > input")).sendKeys(forwardMessage+Keys.ENTER);
				StaticStrings.fluentWait(By.cssSelector(".pane-chat-controls > div:nth-child(1) > div:nth-child(2) > button:nth-child(1)")).click();
				StaticStrings.fluentWait(By.cssSelector("li.menu-item:nth-child(2)")).click();			
				List<WebElement> messageList=driver.findElements(By.cssSelector(".message-list > div"));
				messageList.get(messageList.size()-1).click();
				StaticStrings.fluentWait(By.cssSelector("button.btn-icon:nth-child(4)")).click();

				i=1;
				
			}
			
			
			for(;i<forwardingList.size();i++)
			{
				updateMessage("Forwarding to "+forwardingList.get(i));
				updateProgress(i, forwardingList.size());
				StaticStrings.fluentWait(By.cssSelector("button.menu-item:nth-child(1)")).click();
				WebElement search=driver.findElement(By.cssSelector(".drawer > div:nth-child(3) > div:nth-child(1) > label:nth-child(4) > input:nth-child(1)"));
				search.sendKeys(forwardingList.get(i));			     
				search.sendKeys(Keys.RETURN);
				
				
				try {
					StaticStrings.driver.findElement(By.cssSelector("button.btn-plain:nth-child(2)")).sendKeys(Keys.RETURN);
				} catch (Exception e1) {
					StaticStrings.driver.findElement(By.cssSelector("button.menu-item:nth-child(2)")).click();
					search=driver.findElement(By.cssSelector(".drawer > div:nth-child(3) > div:nth-child(1) > label:nth-child(4) > input:nth-child(1)"));
					search.sendKeys(forwardingList.get(i));			     
					search.sendKeys(Keys.RETURN);
					
					
					try {
						StaticStrings.driver.findElement(By.cssSelector("button.btn-plain:nth-child(2)")).sendKeys(Keys.RETURN);
					} catch (Exception e) {
						
						StaticStrings.driver.findElement(By.cssSelector("button.menu-item:nth-child(3)")).click();
						search=driver.findElement(By.cssSelector(".drawer > div:nth-child(3) > div:nth-child(1) > label:nth-child(4) > input:nth-child(1)"));
						search.sendKeys(forwardingList.get(i));			     
						search.sendKeys(Keys.RETURN);
						
						try {
							StaticStrings.driver.findElement(By.cssSelector("button.btn-plain:nth-child(2)")).sendKeys(Keys.RETURN);
						} catch (Exception e2) {
							search.sendKeys(Keys.ESCAPE);
							search.sendKeys(Keys.ESCAPE);
							StaticStrings.driver.findElement(By.cssSelector(".btn-close-drawer")).click();
							Thread.sleep(500);
							StaticStrings.driver.findElement(By.cssSelector("button.btn-icon:nth-child(1)")).click();
							System.out.println("Element not Found");
						}
						
					}
				
				}
				
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					
				}
				
				if(isCancelled())
				{
					updateMessage("Cancelled");
					updateProgress(1.0,1.0);
					break;
				}
				
				if(i==forwardingList.size()-1)
				{
					updateMessage("done.");
					updateProgress(1.0,1.0);
					
					break;
				}
				
				StaticStrings.fluentWait(By.cssSelector(".pane-chat-controls > div:nth-child(1) > div:nth-child(2) > button:nth-child(1)")).click();
				StaticStrings.fluentWait(By.cssSelector("li.menu-item:nth-child(2)")).click();

				List<WebElement> messageList=driver.findElements(By.cssSelector(".message-list > div"));

				messageList.get(messageList.size()-1).click();

				StaticStrings.fluentWait(By.cssSelector("button.btn-icon:nth-child(4)")).click();
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			updateMessage(e.toString());
			updateProgress(1.0, 1.0);
		}
		return null;

	}
}