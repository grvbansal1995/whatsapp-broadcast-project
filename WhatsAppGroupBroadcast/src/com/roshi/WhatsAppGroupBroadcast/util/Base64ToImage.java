package com.roshi.WhatsAppGroupBroadcast.util;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.roshi.WhatsAppGroupBroadcast.standalone.Logs;

import Decoder.BASE64Decoder;


public class Base64ToImage {

	public void convertBase64ToImage(String sourceData)
	{
		
		String base64Image = sourceData.split(",")[1];
		
		try {
			// create a buffered image
			BufferedImage image = null;
			byte[] imageByte;

			BASE64Decoder decoder = new BASE64Decoder();
			imageByte = decoder.decodeBuffer(base64Image);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();

			// write the image to a file
			File outputfile = new File("resources/image.png");
			ImageIO.write(image, "png", outputfile);
		} catch (IOException e) {
			Logs.writelog(e.toString());
			e.printStackTrace();
		}
		
	}
	
	
}
