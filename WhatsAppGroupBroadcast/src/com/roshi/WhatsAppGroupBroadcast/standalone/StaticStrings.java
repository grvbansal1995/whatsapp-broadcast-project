package com.roshi.WhatsAppGroupBroadcast.standalone;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import net.ucanaccess.jdbc.UcanaccessDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;

public class StaticStrings {
	
	public static       WebDriver              driver;
	public static 		int  			   selectedProfileIndex;
	
	public static WebElement fluentWait(final By locator) {
	    Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(10, TimeUnit.SECONDS)
	            .pollingEvery(1, TimeUnit.SECONDS)
	            .ignoring(NoSuchElementException.class);

	    WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
	        public WebElement apply(WebDriver driver) {
	            return driver.findElement(locator);
	        }
	    });

	    return  foo;
	};
	
	
	public static Connection getDatabeseConnection() throws SQLException,IOException
	{
		Connection c=null;
		final String pathToDB = System.getenv("LOCALAPPDATA")+"\\WhatsApp Broadcast\\wbdb.accdb";
		String url = UcanaccessDriver.URL_PREFIX + pathToDB+";jackcessOpener=com.roshi.WhatsAppGroupBroadcast.standalone.CryptCodecOpener";
		
		try
		{
			c= DriverManager.getConnection(url,"","2499");
			return c;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return c;
		}
		
	}
}
