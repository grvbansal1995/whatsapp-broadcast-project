package com.roshi.WhatsAppGroupBroadcast.standalone;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Logs {

	protected static final String defaultLogFile =System.getenv("LOCALAPPDATA")+"\\WhatsApp Broadcast\\error.log";

	public static void writelog(String s)
	{
		try{
			writelog(defaultLogFile, s);
		}
		catch(IOException ee) {
			System.out.println("Exception - Logs,writeLog - " + ee);
		}
	}

	public static void writelog(String f, String s) throws IOException
	{
		TimeZone    tz  = TimeZone.getTimeZone("IST");
		Date        now = new Date();
		DateFormat  df  = new SimpleDateFormat ("yyyy.MM.dd hh:mm:ss ");
		String      currentTime;
		PrintStream aPrint;
		
		df.setTimeZone(tz);
		currentTime = df.format(now);

		aPrint = new PrintStream(new FileOutputStream(f,true));
		
		aPrint.println(currentTime + " " + s );
		aPrint.flush();
		aPrint.close();
	}
}
