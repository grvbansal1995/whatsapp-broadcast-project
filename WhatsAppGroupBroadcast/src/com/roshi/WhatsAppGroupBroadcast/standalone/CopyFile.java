package com.roshi.WhatsAppGroupBroadcast.standalone;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CopyFile {
	
	
	public static void copyFile(InputStream stream,File dest) throws IOException,FileNotFoundException
	{
		InputStream in=stream;
		FileOutputStream fos=null;
		
		try {
		    fos = new FileOutputStream(dest);
		    byte[] buf = new byte[2048];
		    int r = in.read(buf);
		    while(r != -1) {
		        fos.write(buf, 0, r);
		        r = in.read(buf);
		    }
		} finally {
		    if(fos != null) {
		        fos.close();
		    }
		}
		
	}
	
}
	