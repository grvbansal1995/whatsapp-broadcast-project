package com.roshi.WhatsAppGroupBroadcast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.json.JSONObject;

import com.roshi.WhatsAppGroupBroadcast.model.Profile;
import com.roshi.WhatsAppGroupBroadcast.standalone.CopyFile;
import com.roshi.WhatsAppGroupBroadcast.standalone.Logs;
import com.roshi.WhatsAppGroupBroadcast.standalone.StaticStrings;
import com.roshi.WhatsAppGroupBroadcast.util.ReadFile;
import com.roshi.WhatsAppGroupBroadcast.view.ChooseProfileController;

public class Main extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private ObservableList<Profile> profiles = FXCollections.observableArrayList();
	
	/**
	 * Constructor
	 */
	public Main() {

		File folder = new File(System.getenv("LOCALAPPDATA")+"\\Google\\Chrome\\User Data");
		File[] listOfFiles = folder.listFiles();
		Pattern p;
		Matcher m;
		ReadFile r=new ReadFile();
		String pref,pref_string,profileName;
		JSONObject jsonResult;
		final String REGEX = "Profile (\\d{1,})";

		System.out.println("Profiles:");

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {

			} else if (listOfFiles[i].isDirectory()) {
				String name=listOfFiles[i].getName();
				p = Pattern.compile(REGEX);
				m = p.matcher(name);
				if(m.matches())
				{
					try {
						pref=folder.getAbsolutePath()+"\\"+name+"\\Preferences";
						pref_string=r.readFiledata(pref);
						jsonResult = new JSONObject(pref_string);
						profileName=jsonResult.getJSONObject("profile").getString("name");
						profiles.add(new Profile(profileName,name,FXCollections.observableArrayList()));
					} catch (IOException e) {

						Logs.writelog(e.toString());
						e.printStackTrace();
					}
				}

			}
		}

		profiles.add(new Profile("Default","Default",FXCollections.observableArrayList()));



	}


	public ObservableList<Profile> getProfilesData() {
		return profiles;
	}



	private void loadData()
	{
		try {

			Connection ucaConn=StaticStrings.getDatabeseConnection();
			ResultSet rs;
			int profile_id,list_id,element_id;
			String profileName;
			String list_name,element_name;
			Statement st=ucaConn.createStatement();
			Profile profile;
			ObservableList<ObservableList<String>> list;

			for(int k=0;k<profiles.size();k++)
			{
				profile=profiles.get(k);
				profileName=profile.getProfileName();
				rs=st.executeQuery("SELECT profile_id from Profile WHERE profile_name='"+profileName+"'");
				list=null;
				if(rs.next())
				{
					profile_id=(int)rs.getObject(1);
					list=profile.getList();
					rs=st.executeQuery("SELECT list_id,list_name FROM lists WHERE profile_id="+profile_id+"");

					while(rs.next())
					{

						list_id=(int)rs.getObject(1);
						list_name=(String)rs.getObject(2);
						System.out.println("List_id:"+list_id+" List_name:"+list_name);
						ObservableList<String> temp=FXCollections.observableArrayList();
						temp.add(0,list_name);
						ResultSet rs1;

						rs1=st.executeQuery("SELECT element_id,element_name FROM elements WHERE list_id="+list_id+"");

						while(rs1.next())
						{
							element_id=(int)rs1.getObject(1);
							element_name=(String)rs1.getObject(2);
							System.out.println("Element_id:"+element_id+" Element_name:"+element_name);
							temp.add(element_name);

						}

						list.add(temp);
					}
				}
			}


		} catch (SQLException e) {
			Logs.writelog(e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			Logs.writelog(e.toString());
			e.printStackTrace();
		}

	}


	@Override
	public void start(Stage primaryStage) {

		try {

			InputStream stream;

			File fileRoot=new File(System.getenv("LOCALAPPDATA")+"\\WhatsApp Broadcast");
			fileRoot.mkdirs();

			File chromedriver=new File(fileRoot.getAbsolutePath()+"\\chromedriver.exe");

			if(!chromedriver.exists())
			{
				try {
					stream=Main.class.getResourceAsStream("/resources/chromedriver.exe");
					CopyFile.copyFile(stream,chromedriver.getAbsoluteFile());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


			File wbdb=new File(fileRoot.getAbsolutePath()+"\\wbdb.accdb");

			if(!wbdb.exists())
			{
				try {
					stream=Main.class.getResourceAsStream("/resources/wbdb.accdb");
					CopyFile.copyFile(stream,wbdb.getAbsoluteFile());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


			File ahkHide=new File(fileRoot.getAbsolutePath()+"\\hide.exe");

			if(!ahkHide.exists())
			{
				try {
					stream=Main.class.getResourceAsStream("/resources/hide.exe");
					CopyFile.copyFile(stream,ahkHide.getAbsoluteFile());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			File ahkShow=new File(fileRoot.getAbsolutePath()+"\\show.exe");

			if(!ahkShow.exists())
			{
				try {
					stream=Main.class.getResourceAsStream("/resources/show.exe");
					CopyFile.copyFile(stream,ahkShow.getAbsoluteFile());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			loadData();
			//----------------------------------
			this.primaryStage=primaryStage;
			this.primaryStage.setTitle("WhatsApp Group Broadcast");
			this.primaryStage.getIcons().add(new Image("file:resources/logo.png"));
			initRootLayout();
			showPersonOverview();
		} catch(Exception e) {
			Logs.writelog(e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void stop(){

		//Platform.exit();
		if(StaticStrings.driver!=null)
		{
			StaticStrings.driver.close();
			StaticStrings.driver.quit();

		}
		primaryStage.close();
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() throws IOException {
		// Load root layout from fxml file.
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
		rootLayout = (BorderPane) loader.load();

		// Show the scene containing the root layout.
		Scene scene = new Scene(rootLayout);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	/**
	 * Shows the person overview inside the root layout.
	 */
	public void showPersonOverview() throws IOException{
		// Load person overview.
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/ChooseProfileLayout.fxml"));
		AnchorPane chooseProfileOverview = (AnchorPane) loader.load();

		// Set person overview into the center of root layout.
		rootLayout.setCenter(chooseProfileOverview);

		// Give the controller access to the main app.
		ChooseProfileController controller = loader.getController();
		controller.setMain(this);

	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public BorderPane getRootLayout() {
		return rootLayout;
	}

	public static void main(String[] args) {
		launch(args);
	}
}