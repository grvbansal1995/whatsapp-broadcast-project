package com.roshi.WhatsAppGroupBroadcast.view;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.roshi.WhatsAppGroupBroadcast.Main;
import com.roshi.WhatsAppGroupBroadcast.model.Profile;
import com.roshi.WhatsAppGroupBroadcast.standalone.Logs;
import com.roshi.WhatsAppGroupBroadcast.standalone.StaticStrings;
import com.roshi.WhatsAppGroupBroadcast.util.Base64ToImage;


public class ChooseProfileController {

	Main main;

	@FXML
	private TableView<Profile> profileTable;
	@FXML
	private TableColumn<Profile, String> profileNameColumn;
	@FXML
	private Label selectedProfile;
	@FXML
	private Button proceedButton;
	@FXML
	private ProgressIndicator progressIndicator;
	@FXML
	private Text progressMsg;

	final ImageView iv;

	AnchorPane QRCodeScreen;

	final Text QRScreenLabel;

	final Label QRScreenHeading;

	ObservableList<Profile> profiles;

	String selectedProfileFolder;

	boolean res=false;

	final Service<Boolean> getIn;

	public ChooseProfileController() throws IOException{

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/QRCodeScreenLayout.fxml"));
		QRCodeScreen = (AnchorPane) loader.load();

		QRScreenHeading=(Label)((VBox)QRCodeScreen.getChildren().get(0)).getChildren().get(0);

		iv=(ImageView)((VBox)QRCodeScreen.getChildren().get(0)).getChildren().get(1);
		iv.setImage(new Image("file:resources/loading.gif"));

		QRScreenLabel=(Text)((TextFlow)((VBox)QRCodeScreen.getChildren().get(0)).getChildren().get(2)).getChildren().get(0);

		
		getIn=new Service<Boolean>() {
			
			@Override
			protected Task<Boolean> createTask()
			{
				return new Task<Boolean>() {
					@Override
					protected Boolean call(){
						int i=0;
						System.out.println("Task");
						StaticStrings.driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
						while(StaticStrings.driver.findElements(By.cssSelector(".pane-subheader")).size()==0)
						{
							try {
								updateMessage("Not able to reach your phone. Please Check your internet connection.");
								Thread.sleep(1000);
								i++;

								if(i>10)
								{
									break;
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if(i>10)
						{
							return false;
						}
						else
						{
							return true;
						}

					}
				};

			}
		};
		
				
		getIn.stateProperty().addListener((obVal,oS,nS)->{

			if (nS == Worker.State.SUCCEEDED) {

				try {
					boolean res1=getIn.getValue();

					if(res1)
					{
						showProfile();
					}
					else
					{
						progressMsg.textProperty().unbind();
						progressIndicator.progressProperty().unbind();
						progressIndicator.setVisible(false);
						proceedButton.setText("Retry");
						proceedButton.setDisable(false);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});
	}

	@FXML
	private void initialize() {

		// Initialize the person table with the two columns.
		profileNameColumn.setCellValueFactory(cellData -> cellData.getValue().profileNameProperty());

		// Listen for selection changes and show the person details when changed.
		profileTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) ->
				{
					StaticStrings.selectedProfileIndex=profileTable.getSelectionModel().getSelectedIndex();
					selectedProfile.setText(newValue.getProfileName());
					selectedProfileFolder=newValue.getProfileDirectory();
				});

	}

	@FXML
	private void handleProceedButton() {


		final Task<Boolean> openWhatsApp = new Task<Boolean>() {
			@Override
			protected Boolean call(){

				System.setProperty("webdriver.chrome.driver",System.getenv("LOCALAPPDATA")+"\\WhatsApp Broadcast\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("user-data-dir="+System.getenv("LOCALAPPDATA")+"\\Google\\Chrome\\User Data\\"+selectedProfileFolder);
				StaticStrings.driver=new ChromeDriver(options);

				try
				{
					Runtime.getRuntime().exec(new String[]{System.getenv("LOCALAPPDATA")+"\\WhatsApp Broadcast\\hide.exe"});
				}
				catch(IOException e)
				{
					Logs.writelog(e.getMessage());
					e.printStackTrace();
				}

				StaticStrings.driver.get("http://web.whatsApp.com");
				StaticStrings.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				if(StaticStrings.driver.findElements(By.cssSelector("#window > div.entry-main > div.entry-text > div.entry-subtitle")).size()==0)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
		};


		if (StaticStrings.selectedProfileIndex >= 0) {

			if(proceedButton.getText()=="Retry")
			{
				proceedButton.setDisable(true);
				proceedButton.setText("Connecting...");
				progressMsg.setText("");
				progressMsg.textProperty().bind(getIn.messageProperty());
				progressIndicator.progressProperty().bind(getIn.progressProperty());
				progressIndicator.setVisible(true);
				System.out.println("a");
			/*	Thread t1=new Thread(getIn);
				t1.start();
			*/	
				getIn.reset();
				getIn.start();
				System.out.println("b");
			}
			else
			{
				try
				{

					showProgress(openWhatsApp, ()->showQRCode());
					new Thread(openWhatsApp).start();

				}
				catch(Exception e)
				{
					e.printStackTrace();
				}

			}
		} 
		else 
		{
			// Nothing selected.	
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Profile Selected");
			alert.setContentText("Please select a profile in the table to proceed.");

			alert.showAndWait();
		}

	}

	private void showProgress(Task<Boolean> task,InitCompletionHandler initCompletionHandler)
	{
		progressIndicator.setVisible(true);
		proceedButton.setText("Connecting...");
		proceedButton.setDisable(true);
		task.stateProperty().addListener((observableValue, oldState, newState) -> {
			if (newState == Worker.State.SUCCEEDED) {
				try {
					res=task.get();
				} catch (Exception e) {
					Logs.writelog(e.toString());
					e.printStackTrace();
				}


				initCompletionHandler.complete();
			}
		});

	}

	private void showQRCode()
	{

		final Task<Boolean> QRcodeScan = new Task<Boolean>() {
			@Override
			protected Boolean call(){

				WebDriver driver=StaticStrings.driver;
				WebElement QR;

				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

				while(StaticStrings.driver.findElements(By.cssSelector("#window > div.entry-main > div.entry-text > div.entry-subtitle")).size()!=0)
				{

					try
					{

						if(StaticStrings.driver.findElements(By.cssSelector(".qr-button")).isEmpty())
						{

							QR=driver.findElement(By.cssSelector(".qrcode > img:nth-child(4)"));
							String qrData=QR.getAttribute("src");					
							Base64ToImage base64ToImage=new Base64ToImage();
							base64ToImage.convertBase64ToImage(qrData);
							iv.setImage(new Image("file:resources/image.png"));
						}
						else
						{
							iv.setImage(new Image("file:resources/loading.gif"));
							driver.findElement(By.cssSelector(".qr-button")).click();
							continue;
						}
					}
					catch(Exception e)
					{
						System.out.println("Successful Login");	
					}
				}

				return true;
			}
		};



		
		if(!res)
		{
			Thread t=new Thread(QRcodeScan);
			t.start();

			main.getRootLayout().setCenter(QRCodeScreen);

			QRcodeScan.stateProperty().addListener((observableValue, oldState, newState) -> {
				if (newState == Worker.State.SUCCEEDED) {

					QRScreenLabel.textProperty().bind(getIn.messageProperty());
					QRScreenLabel.setVisible(true);
					QRScreenHeading.setText("Authentication Complete!!");
				/*	Thread t1=new Thread(getIn);
					t1.start();
*/
					getIn.start();
					/*getIn.stateProperty().addListener((obVal,oS,nS)->{

						if (nS == Worker.State.SUCCEEDED) {

							try {
								boolean res1=getIn.get();

								if(res1)
								{
									showProfile();
								}

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}



						}

					});
*/
				}
			});
		}
		else
		{
			System.out.println("Already logged in");	
			progressMsg.textProperty().bind(getIn.messageProperty());
			progressMsg.setVisible(true);

			progressIndicator.progressProperty().unbind();
			progressIndicator.progressProperty().bind(getIn.progressProperty());
			/*Thread t1=new Thread(getIn);
			t1.start();
*/

			getIn.start();
/*			getIn.stateProperty().addListener((obVal,oS,nS)->{

				if (nS == Worker.State.SUCCEEDED) {

					try {
						boolean res1=getIn.get();

						if(res1)
						{
							showProfile();
						}
						else
						{
							progressMsg.textProperty().unbind();
							progressIndicator.progressProperty().unbind();
							progressIndicator.setVisible(false);
							proceedButton.setText("Retry");
							proceedButton.setDisable(false);
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			});
*/		}


	}

	private void showProfile()
	{
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/GroupLayout.fxml"));
			AnchorPane ProfileScreen = (AnchorPane) loader.load();

			main.getRootLayout().setCenter(ProfileScreen);

			GroupController controller = loader.getController();
			controller.setMain(main);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logs.writelog(e.toString());
			e.printStackTrace();
		}

	}


	public interface InitCompletionHandler {
		public void complete(); 
	}

	public void setMain(Main main) {
		this.main = main;
		profiles=main.getProfilesData();
		// Add observable list data to the table
		profileTable.setItems(profiles);
	}

}
