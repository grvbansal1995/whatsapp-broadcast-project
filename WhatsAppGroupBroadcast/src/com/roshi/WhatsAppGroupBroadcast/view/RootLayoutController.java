package com.roshi.WhatsAppGroupBroadcast.view;

import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import com.roshi.WhatsAppGroupBroadcast.standalone.StaticStrings;

public class RootLayoutController {
	
	@FXML
	private MenuBar menu;
	@FXML
	private Menu File;
	@FXML
	private MenuItem Close;
	
	@FXML
	private void initialize() 
	{
	}
	

	@FXML
	private void handleCloseMenuItem()
	{
		if(StaticStrings.driver!=null)
		{
			StaticStrings.driver.close();
			StaticStrings.driver.quit();

		}
		Stage s=(Stage)menu.getScene().getWindow();
		s.close();
	}
	
	

}
