package com.roshi.WhatsAppGroupBroadcast.view;

import java.io.IOException;

import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;

import com.roshi.WhatsAppGroupBroadcast.Main;
import com.roshi.WhatsAppGroupBroadcast.standalone.Logs;
import com.roshi.WhatsAppGroupBroadcast.util.Broadcast;

public class SendingController {
	
	Main main;
	
	Broadcast task;
	
	@FXML
	private ProgressBar progress;
	@FXML
	private Label status;
	@FXML
	private Button cancelButton;
	@FXML
	private Button doneButton;
	
	public SendingController() {
	
	}
	
	
	@FXML
	private void initialize()
	{
	}
	
	@FXML
	private void handleCancelButton()
	{
		task.cancel();
		cancelButton.setDisable(true);
		doneButton.setDisable(false);
		
	}
	
	@FXML
	private void handleDoneButton()
	{

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/SendMessageLayout.fxml"));
			AnchorPane sendScreen = (AnchorPane)loader.load();
			
			main.getRootLayout().setCenter(sendScreen);
			
			SendMessageController controller = loader.getController();
			controller.setMain(main);
		} catch (IOException e) {
			Logs.writelog(e.toString());
			e.printStackTrace();
		}
	}
	
	public void setTask(Broadcast task)
	{
		this.task=task;
		progress.progressProperty().bind(task.progressProperty());
		status.textProperty().bind(task.messageProperty());
		
		task.stateProperty().addListener((observableValue, oldState, newState) -> {
			if (newState == Worker.State.SUCCEEDED) {
				doneButton.setDisable(false);
				cancelButton.setDisable(true);
			}
		});
		
	}
	
	public void setMain(Main main) {
		this.main = main;	
	}

}
