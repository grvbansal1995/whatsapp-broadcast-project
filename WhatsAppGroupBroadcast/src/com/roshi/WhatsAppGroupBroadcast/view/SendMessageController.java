package com.roshi.WhatsAppGroupBroadcast.view;

import java.io.IOException;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import com.roshi.WhatsAppGroupBroadcast.Main;
import com.roshi.WhatsAppGroupBroadcast.model.Profile;
import com.roshi.WhatsAppGroupBroadcast.standalone.Logs;
import com.roshi.WhatsAppGroupBroadcast.standalone.StaticStrings;
import com.roshi.WhatsAppGroupBroadcast.util.Broadcast;

public class SendMessageController {

	@FXML
	private Button sendOption1;
	@FXML
	private Button sendOption2;
	@FXML
	private TextArea option1Field;
	@FXML
	private TextField option2Field;
	@FXML
	private Button backButton;

	Main main;

	ObservableList<ObservableList<String>> list;
	private ObservableList<String> forwardingList;
	int selectedProfileIndex=StaticStrings.selectedProfileIndex;

	public SendMessageController() {

	}


	@FXML
	private void initialize()
	{

	}
	
	@FXML
	private void handleBackButton()
	{
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/GroupLayout.fxml"));
			AnchorPane ProfileScreen = (AnchorPane) loader.load();
			
			main.getRootLayout().setCenter(ProfileScreen);
			
			GroupController controller = loader.getController();
			controller.setMain(main);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			Logs.writelog(e.toString());
			e.printStackTrace();
		}

	}

	@FXML
	private void handleOption1()
	{
		if(option1Field.getText()!=null)
		{
			ChoiceDialog<String> dialog = new ChoiceDialog<String>(null, forwardingList);
			dialog.setTitle("Forwarding Lists");
			dialog.setHeaderText("Choose a Forwarding List");
			dialog.setContentText("Choose your Forwarding List:");

			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()){

				int list_index=forwardingList.indexOf(result.get());
				
				System.out.println("");
				if(list_index<0)
				{
					System.out.println("List not found!!");
					return;
				}

				Broadcast task = new Broadcast(list.get(list_index),option1Field.getText(),1);

				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(Main.class.getResource("view/SendingLayout.fxml"));
					AnchorPane sendingScreen = (AnchorPane)loader.load();

					main.getRootLayout().setCenter(sendingScreen);

					SendingController controller = loader.getController();
					controller.setMain(main);
					controller.setTask(task);
				} catch (IOException e) {
					Logs.writelog(e.toString());
					e.printStackTrace();
				}

				final Thread taskThread = new Thread(task, "Broadcast");
				taskThread.setDaemon(true);
				taskThread.start();    
			}
		}
		else
		{
			// Nothing selected.	
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Message");
			alert.setHeaderText("No Message Entered");
			alert.setContentText("Please enter a message in the space provided.");
			alert.showAndWait();
		}
	}

	@FXML
	private void handleOption2()
	{
		if(option2Field.getText()!=null)
		{
			ChoiceDialog<String> dialog = new ChoiceDialog<String>(null, forwardingList);
			dialog.setTitle("Forwarding Lists");
			dialog.setHeaderText("Choose a Forwarding List");
			dialog.setContentText("Choose your Forwarding List:");

			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()){

				int list_index=forwardingList.indexOf(result.get());

				if(list_index<0)
				{
					System.out.println("List not found!!");
					return;
				}

				Broadcast task = new Broadcast(list.get(list_index),option2Field.getText(),2);

				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(Main.class.getResource("view/SendingLayout.fxml"));
					AnchorPane sendingScreen = (AnchorPane)loader.load();

					main.getRootLayout().setCenter(sendingScreen);

					SendingController controller = loader.getController();
					controller.setMain(main);
					controller.setTask(task);
				} catch (IOException e) {
					Logs.writelog(e.toString());
					e.printStackTrace();
				}

				final Thread taskThread = new Thread(task, "Broadcast");
				taskThread.setDaemon(true);
				taskThread.start();    


			}

		}
		else
		{
			// Nothing selected.	
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Contact");
			alert.setHeaderText("No contact Entered");
			alert.setContentText("Please enter a contact in the space provided.");
			alert.showAndWait();
		}
	}

	public void setMain(Main main) {
		this.main = main;
		Profile profile=main.getProfilesData().get(selectedProfileIndex);
		list=profile.getList();

		forwardingList = FXCollections.observableArrayList();

		for(int i=0;i<list.size();i++)
		{
			forwardingList.add(list.get(i).get(0));
		}

	}

}
