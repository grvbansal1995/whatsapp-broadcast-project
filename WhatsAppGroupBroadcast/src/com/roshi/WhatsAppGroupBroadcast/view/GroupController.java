package com.roshi.WhatsAppGroupBroadcast.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import com.roshi.WhatsAppGroupBroadcast.Main;
import com.roshi.WhatsAppGroupBroadcast.model.Profile;
import com.roshi.WhatsAppGroupBroadcast.standalone.Logs;
import com.roshi.WhatsAppGroupBroadcast.standalone.StaticStrings;

public class GroupController {

	Main main;	
	
	@FXML
	private AnchorPane upper;

	@FXML
	private AnchorPane lower;
	
	@FXML
	private ListView<String> forwardingListTable;
	@FXML
	private TextField addForwardingListField;
	@FXML
	private Button addForwardingListButton;
	@FXML
	private Button deleteForwardingListButton;
	
	@FXML
	private ListView<String> listMemberTable;
	@FXML
	private TextField addListMemberField;
	@FXML
	private Button addListMemberButton;
	@FXML
	private Button deleteListMemberButton;

	@FXML
	private Button next;

	@FXML
	private Label selectedProfile;
	
	int selectedProfileIndex=StaticStrings.selectedProfileIndex;
	Profile profile;
	
	ObservableList<ObservableList<String>> list;
	
	private ObservableList<String> forwardingList;
	private ObservableList<String> memberList;
	

	public GroupController() {

	}

	@FXML
	private void initialize() {
		
		forwardingListTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) ->
				{ 
					if(forwardingListTable.getSelectionModel().getSelectedIndex()>=0)
					{
						memberList.clear();
						memberList.setAll(list.get(forwardingListTable.getSelectionModel().getSelectedIndex()));
						memberList.remove(0);	
						lower.setDisable(false);
					}
					else
					{
						memberList.clear();
						lower.setDisable(true);
					}
				});
	}

	
	@FXML
	private void handleDeleteForwardingListButton() {
		int selectedIndex = forwardingListTable.getSelectionModel().getSelectedIndex();

		if (selectedIndex >= 0) {
			list.remove(selectedIndex);
			
			makeForwardingList();
		} else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAddForwardingListButton() {

		if(addForwardingListField.getText()!=null)
		{
			if(checkDuplicateInForwardingList(addForwardingListField.getText()))
			{
				// Nothing selected.
				Alert alert = new Alert(AlertType.WARNING);
				alert.initOwner(main.getPrimaryStage());
				alert.setTitle("Duplicate");
				alert.setHeaderText("Duplicate List");
				alert.setContentText("A list with the entered name already exists");
				alert.showAndWait();
				
				addForwardingListField.clear();
				return;
			}
			
			ObservableList<String> temp = FXCollections.observableArrayList();
			temp.add(0,addForwardingListField.getText());
			list.add(temp);
			addForwardingListField.clear();
			makeForwardingList();
		}
		else
		{
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Input in text field");
			alert.setHeaderText("No Input");
			alert.setContentText("Please enter group name.");
			alert.showAndWait();
		}
	}

	
	
	@FXML
	public void handleAddForwardingListField()
	{
		handleAddForwardingListButton();
	}
	
	
	@FXML
	private void handleDeleteListMemberButton() {
		int selectedIndex = listMemberTable.getSelectionModel().getSelectedIndex();

		if (selectedIndex >= 0) {
			list.get(forwardingListTable.getSelectionModel().getSelectedIndex()).remove(listMemberTable.getSelectionModel().getSelectedIndex()+1);
			makeMemberList();
		} else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAddListMemberButton() {

		if(addListMemberField.getText()!=null)
		{
			if(checkDuplicateInMemberList(addListMemberField.getText()))
			{
				// Nothing selected.
				Alert alert = new Alert(AlertType.WARNING);
				alert.initOwner(main.getPrimaryStage());
				alert.setTitle("Duplicate");
				alert.setHeaderText("Duplicate Member");
				alert.setContentText("A member with the entered name already exists");
				alert.showAndWait();
				addListMemberField.clear();
				return;
			}
		
			list.get(forwardingListTable.getSelectionModel().getSelectedIndex()).add(addListMemberField.getText());
			addListMemberField.clear();
			makeMemberList();
		}
		else
		{
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("No Input in text field");
			alert.setHeaderText("No Input");
			alert.setContentText("Please enter group name.");
			alert.showAndWait();
		}
	}

	@FXML
	public void handleAddListMemberField()
	{
		handleAddListMemberButton();
	}
	
	@FXML
	private void handleNextButton() {
		
		try {
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/SendMessageLayout.fxml"));
			AnchorPane sendScreen = (AnchorPane)loader.load();
			
			main.getRootLayout().setCenter(sendScreen);
			
			SendMessageController controller = loader.getController();
			controller.setMain(main);
			
			Connection ucaConn=StaticStrings.getDatabeseConnection();
			Statement batch = ucaConn.createStatement();
			ucaConn.setAutoCommit(false);
			ResultSet rs;
			int profile_id;
			int list_id;
			
			Statement st=ucaConn.createStatement();
			
			rs=st.executeQuery("SELECT profile_id from Profile WHERE profile_name='"+selectedProfile.getText()+"'");
			
			if(rs.next())
			{
				profile_id=(int)rs.getObject(1);
				
				ResultSet rs1=st.executeQuery("SELECT list_id from lists WHERE profile_id="+profile_id+"");
				while(rs1.next())
				{
					list_id=(int)rs1.getObject(1);		
					st.execute("DELETE * from elements where list_id="+list_id+"");
					st.execute("DELETE * from lists where list_id="+list_id+"");
				}
				
			}
			else
			{
				st.execute("INSERT INTO Profile(profile_name) VALUES('"+selectedProfile.getText()+"')");
				rs=st.executeQuery("SELECT profile_id from Profile WHERE profile_name='"+selectedProfile.getText()+"'");
				rs.next();
				profile_id=(int)rs.getObject(1);
			}
			
			for(int i=0;i<list.size();i++)
			{
				ObservableList<String> temp=list.get(i);
				st.execute("INSERT INTO lists(list_name,profile_id) VALUES('"+temp.get(0)+"',"+profile_id+")");
				rs=st.executeQuery("SELECT list_id from lists WHERE list_name='"+temp.get(0)+"' and profile_id="+profile_id+"");
				rs.next();
				list_id=(int)rs.getObject(1);
			
				for(int j=1;j<temp.size();j++)
				{
					batch.addBatch("INSERT INTO elements(list_id,element_id,element_name,element_type) VALUES("+list_id+","+j+",'"+temp.get(j)+"','')");
				}
				
			}
			
			batch.executeBatch();
			ucaConn.commit();
			
		} catch (Exception e) {
			Logs.writelog(e.toString());
			e.printStackTrace();
		}
		
	}
	
	private boolean checkDuplicateInForwardingList(String check)
	{
		if(forwardingList.contains(check))
			return true;
		else
			return false;
	}
	
	private boolean checkDuplicateInMemberList(String check)
	{
		if(memberList.contains(check))
			return true;
		else
			return false;
	}
	
	private void makeForwardingList()
	{
		forwardingList.clear();
		
		for(int i=0;i<list.size();i++)
		{
			forwardingList.add(list.get(i).get(0));
		}
		
	}

	
	private void makeMemberList()
	{
		memberList.clear();
		memberList.setAll(list.get(forwardingListTable.getSelectionModel().getSelectedIndex()));
		memberList.remove(0);
	}
	

	public void setMain(Main main) {
		
		this.main = main;
		profile=main.getProfilesData().get(selectedProfileIndex);
		list=profile.getList();
		
		forwardingList = FXCollections.observableArrayList();
		memberList = FXCollections.observableArrayList();
		
		for(int i=0;i<list.size();i++)
		{
			forwardingList.add(list.get(i).get(0));
		}
		
		selectedProfile.setText(profile.getProfileName());
		
		forwardingListTable.setItems(forwardingList);
		listMemberTable.setItems(memberList);
		lower.setDisable(true);
	}

}
