package com.roshi.WhatsAppGroupBroadcast.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class Profile {
	
	private final StringProperty profileName;
	
	private final String profileDirectory;
	
	private final ObservableList<ObservableList<String>>  list;
		
	 /**
     * Constructor with some initial data.
     * 
     */
    public Profile(String profileName,String profileDirectory ,ObservableList<ObservableList<String>> list) {
        
    	this.profileName = new SimpleStringProperty(profileName);
        this.profileDirectory=profileDirectory;
    	this.list = list;
    }

    public String getProfileDirectory()
    {
    	return profileDirectory;
    }
    
    public String getProfileName()
    {
    	return profileName.get();
    }
    
    public void setProfileName(String profileName)
    {
    	 this.profileName.set(profileName);
    }
    
    public StringProperty profileNameProperty()
    {
    	return profileName;
    }
    
    
    public ObservableList<ObservableList<String>> getList()
    {
    	return list;
    }
    
        
}
